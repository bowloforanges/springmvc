package com.softtek.academy.spring.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.softtek.academy.spring.model.Person;

import org.springframework.stereotype.Repository;


@Repository("personRepository")
public class PersonRepository implements PersonDao {

	@PersistenceContext
	private EntityManager entityManager;

    public int saveStudent(String name, int age, int id) {
        // Saves a new Student

        Person p = new Person();
        p.setId(id);
        p.setName(name);
        p.setAge(age);

        entityManager.persist(p);
        
        return 0;
        
    }

    public Person update(Person p) {
        // Updates a student record
        
        Person toUpdate = new Person();

        toUpdate = entityManager.find(Person.class, p.getId());
        toUpdate.setName(p.getName());
        toUpdate.setAge(p.getAge());

        entityManager.merge(toUpdate);

        return p;
    }

    public int delete(int id) {
        // Deletes a student record

        entityManager.remove(entityManager.find(Person.class, id));

        return 0;
    }

    public Person getStudentById(int id) {
        // Retrieves a Student based on their id

        return entityManager.find(Person.class, id);
    }

    public List<Person> getStudents() {
        // Retrieves a list of all students

        return (List<Person>) entityManager.createQuery("SELECT p FROM Person p", Person.class).getResultList();
    }



}