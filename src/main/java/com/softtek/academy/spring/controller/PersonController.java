package com.softtek.academy.spring.controller;

import java.util.List;

import com.softtek.academy.spring.model.Person;
import com.softtek.academy.spring.service.PersonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PersonController {

    @Autowired
    PersonService personService;// Inject dao

    //Retrieves index.jsp
    @RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView index() { 
        return new ModelAndView("index");
    }

    //Retrieves the add student form
    @RequestMapping(value="/personForm", method = RequestMethod.GET)
    public ModelAndView student() { 
        return new ModelAndView("personForm", "command", new Person());
    }

    //Saves new student, then returns to students list
    @RequestMapping(value = "/addPerson", method = RequestMethod.POST)
    public String save(@ModelAttribute("student") Person student) {

        personService.saveStudent(student.getName(), student.getAge(), student.getId());

        return "redirect:/personList";

    }

    //Retrieves students list
    @RequestMapping("/personList")
    public String viewStudent(ModelMap m) {

        List<Person> list = personService.getStudents();
        m.addAttribute("list", list);

        return "personList";
    }

    //Retrieves the edit student form based on student's id
    @RequestMapping(value = "/edit/{id}")
    public String edit(@PathVariable int id, Model m) {
        Person student = personService.getStudentById(id);
        m.addAttribute("command", student);
        return "studentEditForm";
    }

    //Saves an entry after it has been edited
    @RequestMapping(value = "/editsave", method = RequestMethod.POST)
    public String editSave(@ModelAttribute("student") Person student) {
        personService.update(student);
        return "redirect:/personList";
    }

    //Deletes based on student's id
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable int id) {
        personService.delete(id);
        return "redirect:/personList";
    }

}
