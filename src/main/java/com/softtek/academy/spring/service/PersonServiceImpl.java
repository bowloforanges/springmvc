package com.softtek.academy.spring.service;

import java.util.List;

import com.softtek.academy.spring.model.Person;
import com.softtek.academy.spring.dao.PersonDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    @Autowired
    @Qualifier("personRepository")
    private PersonDao personDao;

    public PersonServiceImpl(PersonDao personDao){
        this.personDao = personDao;
    }

    public int saveStudent(String name, int age, int id) {
        // Delegates method to Dao 

        return personDao.saveStudent(name, age, id);
    }

    public Person update(Person p) {
        // Delegates method to Dao

        return personDao.update(p);
    }

    public int delete(int id) {
        // Delegates method to Dao

        return personDao.delete(id);
    }

    public Person getStudentById(int id) {
        // Delegates method to Dao

        return personDao.getStudentById(id);
    }

    public List<Person> getStudents() {
        // Delegates method to Dao
        
        return personDao.getStudents();
    }
    
}