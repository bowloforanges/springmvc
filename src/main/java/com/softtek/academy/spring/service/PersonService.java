package com.softtek.academy.spring.service;

import java.util.List;

import com.softtek.academy.spring.model.Person;

public interface PersonService {

    int saveStudent(String name, int age, int id);
    Person update(Person p);
    int delete(int id);
    Person getStudentById(int id);
    List<Person> getStudents();

}