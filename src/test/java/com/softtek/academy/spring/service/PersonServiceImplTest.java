package com.softtek.academy.spring.service;

import static org.junit.Assert.assertEquals;

import com.softtek.academy.spring.configuration.SpringMVCConfiguration;
import com.softtek.academy.spring.model.Person;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringMVCConfiguration.class})
@WebAppConfiguration
public class PersonServiceImplTest {

    @Autowired
    private PersonService ps;

    @Test
    public void getByIdAndCheckNameTest(){
        //Setup
        int idToGet = 1;
        Person p = ps.getStudentById(idToGet);
        String expectedName = "SAS"; //Modify accordingly to your database, mine is SAS at id 1.

        //Execute
        String actualName = p.getName();

        //Verify
        assertEquals(expectedName, actualName);
    }

}
