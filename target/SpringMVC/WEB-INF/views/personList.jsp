<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import = "java.util.*" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
        <table>
            <tr>
                <td>ID</td>
                <td>NAME</td>
                <td>AGE</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
        <c:forEach items="${list}" var="student">
            <tr>
                <td>${student.getId()}</td>
                <td>${student.getName()}</td>
                <td>${student.getAge()}</td>
                <td><a href="./edit/${student.getId()}">Edit</a></td>
                <td><a href="./delete/${student.getId()}">Delete</a></td>
            </tr>
        </c:forEach>        
        </table>

</body>
</html>